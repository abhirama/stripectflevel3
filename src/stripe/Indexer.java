package stripe;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 9:29 PM
 */
public class Indexer {
    private static String root;

    private static boolean indexingDone = false;

    private static Map<String, Set<String>> trigramFiles = new HashMap<String, Set<String>>();

    public static void index(Collection<String> files) throws IOException {
        System.out.println(">>>>>>>>>>>>>>>>>>>>Indexing started");

        for (String filePath : files) {
            File file = new File(filePath);

            System.out.println("Indexing file - " + file.getAbsolutePath());

            if (file.getName().startsWith(".")) {
                System.out.println("Skipping file - " + file);
                continue;
            }

            List<String> contents = FileUtils.readLines(file);
            final String relativePath = relativize(file.getPath());

            for (String content : contents) {
                String splits[] = content.split("\\s+");

                for (String split : splits) {

                    Set<String> trigrams = Util.trigrams(split);

                    for (String trigram : trigrams) {
                        if ("".equals(trigram) || ".".equals(trigram)) {
                            continue;
                        }

                        if (!trigramFiles.containsKey(trigram)) {
                            Set<String> trigramFiles = new HashSet<String>();
                            Indexer.trigramFiles.put(trigram, trigramFiles);
                        }

                        trigramFiles.get(trigram).add(relativePath);

                    }
                }
            }
        }

        System.out.println(">>>>>>>>>>>>>>>>>>>>Indexing done");
        indexingDone = true;
    }

    public static List<String> search(String query) {
        List<String> fileLines = new LinkedList<String>();

        Set<String> trigrams = Util.trigrams(query);

        final String first = trigrams.iterator().next();

        if (trigramFiles.containsKey(first)) {
            Set<String> filePaths = new HashSet<String>(trigramFiles.get(first));

            for (String trigram : trigrams) {
                if (trigramFiles.containsKey(trigram)) {
                    filePaths.retainAll(trigramFiles.get(trigram));
                }
            }

            for (String filePath : filePaths) {
                File file = new File(root, filePath);

                List<Integer> lineNumbers = getLineNo(file, query);

                for (Integer lineNumber : lineNumbers) {
                    fileLines.add(filePath + ":" + lineNumber);
                }

            }
        }

        return fileLines;
    }

    private static String relativize(String path) {
        return new File(root).toURI().relativize(new File(path).toURI()).getPath();
    }

    public static void setRoot(String root) {
        Indexer.root = root;
    }

    public static void clean() {
        trigramFiles.clear();
    }

    public static boolean isIndexingDone() {
        return indexingDone;
    }

    private static void serialize(Map<String, Set<Integer>> lines, String fileName) {
        fileName = fileName.replace("/", "");
        try {
            FileOutputStream fileOut = new FileOutputStream(new File("/tmp", fileName));
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(lines);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            throw new RuntimeException(i);
        }
    }

    private static Map<String, Set<Integer>> deserialize(String fileName) {
        fileName = fileName.replace("/", "");
        try {
            FileInputStream fileIn = new FileInputStream(new File("/tmp", fileName));
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Map<String, Set<Integer>> lines = (Map<String, Set<Integer>>) in.readObject();
            in.close();
            fileIn.close();
            return lines;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Integer> getLineNo(File file, String needle) {
        BufferedReader br = null;

        List<Integer> lineNumbers = new LinkedList<Integer>();

        try {
            String line;

            br = new BufferedReader(new FileReader(file));

            int lineNo = 1;

            while ((line = br.readLine()) != null) {
                if (line.contains(needle)) {
                    lineNumbers.add(lineNo);
                }

                lineNo = lineNo + 1;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return lineNumbers;
    }

    public static void main(String[] args) throws IOException {
        Collection<File> files = FileUtils.listFiles(new File("/home/abhi/Projects/personal/stripeCtf/level30/test/data/input"), null, true);

        List<String> filePaths = new LinkedList<String>();

        for (File file : files) {
            filePaths.add(file.getAbsolutePath());
        }

        Indexer.setRoot("/home/abhi/Projects/personal/stripeCtf/level30/test/data/input");

        long start = System.currentTimeMillis();

        Indexer.index(filePaths);

        long end = System.currentTimeMillis();

        System.out.println("Indexing time - " + (end - start));

        System.out.println(Indexer.search("dermestoid"));
    }
}
