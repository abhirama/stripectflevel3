package stripe;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 10:07 PM
 */
public class HealthCheckHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        Map<String, String> map = new HashMap<String, String>();
        map.put("success", "true");

        String json = new JSONSerializer().deepSerialize(map);

        response.getWriter().println(json);
    }
}
