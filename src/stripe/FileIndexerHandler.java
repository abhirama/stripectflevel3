package stripe;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: abhi
 * Date: 27/1/14
 * Time: 10:24 AM
 */
public class FileIndexerHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String files = request.getParameter("files");
        String root = request.getParameter("root");

        System.out.println("Files param - " + files);
        System.out.println("Root - " + root);

        Indexer.clean();

        final List<String> fileList = new JSONDeserializer<List<String>>().deserialize(files);

        Indexer.setRoot(root);

        new Thread() {
            public void run() {
                try {
                    System.out.println("Starting to index files");
                    Indexer.index(fileList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        Map<String, String> map = new HashMap<String, String>();
        map.put("success", "true");

        String json = new JSONSerializer().deepSerialize(map);

        response.getWriter().println(json);
    }
}
