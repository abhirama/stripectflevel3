package stripe;

import org.apache.commons.cli.*;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 9:05 PM
 */
public class Bootstrapper {
    private static boolean master = false;

    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option masterOption = OptionBuilder.withArgName("master")
                .withLongOpt("master")
                .withType(String.class)
                .withDescription("start date")
                .isRequired(false)
                .create("master");
        options.addOption(masterOption);

        Option idOption = OptionBuilder.withArgName("id")
                .withLongOpt("id")
                .withType(String.class)
                .hasArg()
                .withDescription("start date")
                .isRequired(false)
                .create("id");
        options.addOption(idOption);

        int port = 9090;

        CommandLineParser parser = new PosixParser();

        try {
            CommandLine commandLine = parser.parse(options, args);
            String masterArgument = commandLine.getOptionValue("master");


            String stringId = commandLine.getOptionValue("id");

            if (stringId != null && !"".equals(stringId)) {
                int id = Integer.parseInt(stringId);

                port = port + id;
            } else {
                master = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ContextHandler healthCheckHandler = new ContextHandler("/healthcheck");
        healthCheckHandler.setHandler(new HealthCheckHandler());
        healthCheckHandler.setAllowNullPathInfo(true);

        ContextHandler indexHandler = new ContextHandler("/index");
        indexHandler.setHandler(new IndexHandler());
        indexHandler.setAllowNullPathInfo(true);

        ContextHandler searchHandler = new ContextHandler("/");
        searchHandler.setHandler(new SearchHandler());
        searchHandler.setAllowNullPathInfo(true);

        ContextHandler isIndexedHandler = new ContextHandler("/isIndexed");
        isIndexedHandler.setHandler(new IsIndexedHandler());
        isIndexedHandler.setAllowNullPathInfo(true);

        ContextHandler fileIndexHandler = new ContextHandler("/indexFiles");
        fileIndexHandler.setHandler(new FileIndexerHandler());
        fileIndexHandler.setAllowNullPathInfo(true);

        ContextHandlerCollection contexts = new ContextHandlerCollection();
        contexts.setHandlers(new Handler[] {healthCheckHandler, indexHandler, searchHandler, isIndexedHandler, fileIndexHandler});

        Server server = new Server(port);

        server.setHandler(contexts);

        server.start();
        server.join();
    }

    public static boolean isMaster() {
        return master;
    }
}
