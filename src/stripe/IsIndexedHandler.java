package stripe;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 11:03 PM
 */
public class IsIndexedHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        Map<String, Boolean> responseMap = new LinkedHashMap<String, Boolean>();

        if (Bootstrapper.isMaster()) {
            List<Integer> ports = new ArrayList<Integer>(3);
            ports.add(9091);
            ports.add(9092);
            ports.add(9093);

            List<ResponseThread> responseThreads = new ArrayList<ResponseThread>(3);
            List<Thread> executionThreads = new ArrayList<Thread>(3);

            for (final Integer port : ports) {
                ResponseThread responseThread = new ResponseThread();
                responseThreads.add(responseThread);

                responseThread.port = port;

                Thread thread = new Thread(responseThread);
                executionThreads.add(thread);

                thread.start();
            }

            for (Thread executionThread : executionThreads) {
                try {
                    executionThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (ResponseThread responseThread : responseThreads) {
                Map<String, Boolean> responseJson = new JSONDeserializer<Map<String, Boolean>>().deserialize(responseThread.response);
                if (!responseJson.get("success")) {
                    responseMap.put("success", false);
                    break;
                } else {
                    responseMap.put("success", true);
                }
            }
        } else {
            responseMap.put("success", Indexer.isIndexingDone());
        }

        String json = new JSONSerializer().deepSerialize(responseMap);

        response.getWriter().println(json);
    }

    private static String get(int port) {
        try {
            return org.apache.http.client.fluent.Request.Get(String.format("http://localhost:%d/isIndexed", port)).execute().returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static class ResponseThread implements Runnable {
        int port;
        String query;
        String response;

        @Override
        public void run() {
            response = get(port);
        }
    }
}
