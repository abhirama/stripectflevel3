package stripe;

import flexjson.JSONSerializer;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.fluent.Form;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 10:17 PM
 */
public class IndexHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String path = request.getParameter("path");

        System.out.println("Path passed is - " + path);

        final Collection<File> files = FileUtils.listFiles(new File(path), null, true);

        List<String> list0 = new LinkedList<String>();
        List<String> list1 = new LinkedList<String>();
        List<String> list2 = new LinkedList<String>();

        Iterator<File> fileIterator = files.iterator();

        while (fileIterator.hasNext()) {
            File file0 = fileIterator.next();
            list0.add(file0.getAbsolutePath());

            if (fileIterator.hasNext()) {
                File file1 = fileIterator.next();
                list1.add(file1.getAbsolutePath());

                if (fileIterator.hasNext()) {
                    File file2 = fileIterator.next();
                    list2.add(file2.getAbsolutePath());
                }
            }
        }

        System.out.println("List 0 - " + list0);
        System.out.println("List 1 - " + list1);
        System.out.println("List 2 - " + list2);


        System.out.println("Starting file indexing");

        post(list0, path, 9091);
        post(list1, path, 9092);
        post(list2, path, 9093);

        System.out.println("Done file indexing");

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        Map<String, String> map = new HashMap<String, String>();
        map.put("indexingSuccess", "true");

        String json = new JSONSerializer().deepSerialize(map);

        response.getWriter().println(json);
    }

    public void post(List<String> files, String root, int port) {
        try {
            org.apache.http.client.fluent.Request.Post(String.format("http://localhost:%d/indexFiles", port))
                    .bodyForm(Form.form().add("files", new JSONSerializer().deepSerialize(files))
                            .add("root", root).build())
                    .execute().returnContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        IndexHandler indexHandler = new IndexHandler();
        indexHandler.post(new LinkedList<String>(), "/", 9091);
    }
}
