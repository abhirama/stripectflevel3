package stripe;

import java.util.HashSet;
import java.util.Set;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 9:44 PM
 */
public class Util {

    public static Set<String> trigrams(String word) {
        Set<String> foo = new HashSet<String>();

        for (int i = 0;;++i) {
            try {
                String sub = word.substring(i, i + 3);
                foo.add(sub);
            } catch (Exception e) {
                break;
            }
        }

        return foo;
    }

    public static void main(String[] args) {
        System.out.println(Util.trigrams("w"));
    }
}
