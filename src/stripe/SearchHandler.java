package stripe;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.apache.http.client.fluent.Form;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * User: abhi
 * Date: 26/1/14
 * Time: 10:26 PM
 */
public class SearchHandler extends AbstractHandler {
    @Override
    public void handle(String s, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        final String query = request.getParameter("q");

        List<String> results = new LinkedList<String>();

        if (Bootstrapper.isMaster()) {
            List<Integer> ports = new ArrayList<Integer>(3);
            ports.add(9091);
            ports.add(9092);
            ports.add(9093);
            
            List<ResponseThread> responseThreads = new ArrayList<ResponseThread>(3);
            List<Thread> executionThreads = new ArrayList<Thread>(3);

            for (final Integer port : ports) {
                ResponseThread responseThread = new ResponseThread();
                responseThreads.add(responseThread);

                responseThread.query = query;
                responseThread.port = port;

                Thread thread = new Thread(responseThread);
                executionThreads.add(thread);

                thread.start();
            }

            for (Thread executionThread : executionThreads) {
                try {
                    executionThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (ResponseThread responseThread : responseThreads) {
                Map<String, Object> responseJson = new JSONDeserializer<Map<String, Object>>().deserialize(responseThread.response);
                List<String> searchResult = ((List<String>) responseJson.get("results"));
                results.addAll(searchResult);
            }
        } else {
            results.addAll(Indexer.search(query));
        }

        Map<String, Object> jsonResponse = new LinkedHashMap<String, Object>();

        if (results.isEmpty()) {
            jsonResponse.put("success", false);
            jsonResponse.put("results", results);
        } else {
            jsonResponse.put("success", true);
            jsonResponse.put("results", results);
        }

        String json = new JSONSerializer().deepSerialize(jsonResponse);

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        response.getWriter().println(json);
    }

    private static String get(int port, String query) {
        try {
            return org.apache.http.client.fluent.Request.Get(String.format("http://localhost:%d/?q=%s", port, query)).execute().returnContent().asString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static class ResponseThread implements Runnable {
        int port;
        String query;
        String response;

        @Override
        public void run() {
            response = get(port, query);
        }
    }

}
